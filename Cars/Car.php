<?php

namespace Cars;
use Cars\Interfaces\IRepair;

abstract class Car implements IRepair
{
    protected $NameOfModel;
    protected $Year;
    protected $Color;
    protected $Condition;

    public function __construct($name, $year, $color, $condition)
    {
        $this->NameOfModel = $name;
        $this->Year = $year;
        $this->Color = $color;
        $this->Condition = $condition;
        $logger = new \Katzgrau\KLogger\Logger(__DIR__.'/../logs');
        $logger->info('File was created!!!');
    }


    public function getNameOfModel()
    {
        return $this->NameOfModel;
    }

    public function getYear()
    {
        return $this->Year;
    }

    public function getColor()
    {
        return $this->Color;
    }

    public function setColor($C)
    {
        $this->Color = $C;
    }

    public function getCondition()
    {
        return $this->Condition;
    }

    public function setCondition($C)
    {
        $this->Condition = $C;
    }

    public function MadeAndPrint()
    {

        echo "Name of this Model: ", $this->getNameOfModel(), "\n";
        echo "Year of Appearing this car: ", $this->getYear(), "\n";
        echo "Color of this car: ", $this->getColor(), "\n";
        echo "Condition of this car: ", $this->getCondition(), "\n";
    }

    public function Paint($NewColor)
    {
        echo "Painting...\n";
        return $this->setColor($NewColor);
    }

    public function Repair($NewCondition)
    {
        echo "Repairs...\n";
        return $this->setCondition($NewCondition);
    }

}