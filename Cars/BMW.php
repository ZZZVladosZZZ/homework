<?php
namespace Cars;
use Cars\Interfaces\IRepair;

class BMW extends Car implements IRepair
{
    public function __construct($name, $year, $color, $codition)
    {
        parent::__construct($name, $year, $color, $codition);
    }

    public function Paint($NewColor)
    {
        return parent::Paint($NewColor);
    }

    public function Repair($NewCondition)
    {
        return parent::Repair($NewCondition);
    }

    public function MadeAndPrint()
    {
        echo "Car is BMW\n";
        parent::MadeAndPrint();
    }
}
