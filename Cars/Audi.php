<?php
namespace Cars;
use Cars\Interfaces\IRepair;

class Audi extends Car implements IRepair
{
    public function __construct($name, $year, $color, $condition)
    {
        parent::__construct($name, $year, $color, $condition);
    }

    public function Paint($NewColor)
    {
        return parent::Paint($NewColor);
    }

    public function Repair($NewCondition)
    {
        return parent::Repair($NewCondition);
    }

    public function MadeAndPrint()
    {
        echo "Car is Audi\n";
        parent::MadeAndPrint();
    }

}
