<?php

namespace Cars;

use Cars\Interfaces\IRepair;

class ConsernBMW extends BMW implements IRepair
{
    public $engin;

    public function __construct($name, $year, $color, $codition,$engin)
    {
        parent::__construct($name, $year, $color, $codition);
        $this->engin = $engin;
    }

    public function getEngin()
    {
        return $this->engin;
    }

    public function HorsePower()
    {

        echo "Horse Power:",$this->getEngin()*100,"HP\n";
        return $this->getEngin()*100;
    }
    public function MadeAndPrint()
    {
        parent::MadeAndPrint();
        echo "Engin power: L",$this->getEngin(),"\n";
    }
}