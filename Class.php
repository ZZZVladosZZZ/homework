<?php

require 'vendor/autoload.php';

//Showing first Object
echo "\n";
$c = new Cars\ConsernBMW("M5", 2009, "Blue", "Broken",5.8);
$c->Paint("Red");
$c->Repair("Repaired");
$c->MadeAndPrint();
$c->Horsepower();

echo "\n";

$Car2 = new Cars\ConsernAudi("A8", 2018, "Black", "new",2.3);
$Car2->Paint("White");
$Car2->Repair("Well-Worn!!!");
$Car2->MadeAndPrint();
$Car2->HorsePower();
